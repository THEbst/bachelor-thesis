# README

## Setup

```
pip install pre-commit pip-tools
pre-commit install
pip install -r requirements.txt
```

After updating dependencies

```
pip-compile --generate-hashes requirements.in
```
